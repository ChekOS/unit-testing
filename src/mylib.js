/**
 * This arrow function returns the sum of two attributes
 * @param {*} param1 
 * @param {*} param2 
 * @returns 
 */
const add = (param1, param2) => {
    const result = param1 + param2;
    return result
}

/**
 * Without curly brackets it does not require return statement
 * @param {*} a 
 * @param {*} b 
 * @returns 
 */
const substract = (a, b) => a - b;

module.exports = {
    add,
    sub: substract,
    random: () => Math.random(),
    arrayGen: () => [1, 2, 3],
}