const expect = require('chai').expect;
const { assert } = require('chai');
const mylib = require('../src/mylib');

describe('Unit testing mylib.js', () => {

    before(() => {
        ending = 'Test is finished';
        console.log('Test is started');
    })

    it('Should return 2 when using sum function with parameters a=1, b=1', () => {
        const result = mylib.add(1, 1);
        expect(result).to.equal(2);
    })

    it('Assert result of mylib.random() is not a number', () => {
        const result = mylib.random();
        assert(isNaN(result), 'Result is ' + typeof result);
    });

    after(() => {
        console.log(ending);
    })

})